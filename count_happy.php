<?php

$num1=$_POST['num1'];
$num2=$_POST['num2'];

if (rules($num1,$num2))
{
    $result = 'Number of tickets '. count_happy_func($num1,$num2);
    echo $result;
}
else
{
    echo ('Input correct numbers, please');
}

function rules($num1,$num2)
{
   return ( exists_num($num1,$num2) & size($num1) & size($num2) & minmax($num1, $num2));
}

function exists_num($num1,$num2)
{
    return (is_null($num1)||is_null($num2)==false );
}

function size($input_number)
{
    return ($input_number <= 999999 & $input_number >= 100000) ;
}

function minmax($num1, $num2)
{
    if ($num1 <= $num2) {
        return true;
    } else {
        return false;
    }
}

function count_happy_func($num1, $num2)
{
    $count_happy = 0;
    for ($i = $num1; $i <= $num2; $i++) {
        $strn = strval($i);
        if ($strn[0] + $strn[1] + $strn[2] === $strn[3] + $strn[4] + $strn[5]) {
            $count_happy++;
        }
    }
    return $count_happy;
}